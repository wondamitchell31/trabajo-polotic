from django.contrib.auth.models import User
from django.db.models.fields import DateTimeField
from django.shortcuts import redirect, render, get_object_or_404
from django.http.response import HttpResponseRedirect
from .models import Categoria, Producto , Carrito
from django.urls import reverse
from django.contrib.auth.decorators import login_required
# Create your views here.


def inicio(request):
    listaCategorias = Categoria.objects.all()
    listaProductos = Producto.objects.all().order_by('-id')
    top3 = listaProductos[:3]
    top10 = listaProductos[3:10]
    return render(request, "Jaguarete/principal.html",{"listcat":listaCategorias, "prod3":top3, "prod10":top10})

def login(request):
    listaCategorias = Categoria.objects.all()
    return render(request, "Jaguarete/login.html",{"listcat":listaCategorias})

def registro(request):
    listaCategorias = Categoria.objects.all()
    return render(request, "Jaguarete/registro.html",{"listcat":listaCategorias})

def busqueda(request):
    listaCategorias = Categoria.objects.all()
    #listaProductos = Producto.objects.all().filter('self.categoria')
    return render(request, "Jaguarete/busqueda.html",{"listcat":listaCategorias})

#Pagina de busqueda por categoria
def filtro_cat(request,cat_id):
    listaCategorias = Categoria.objects.all()
    this_cat = get_object_or_404(Categoria,id=cat_id)
    listaProductos = Producto.objects.all().filter(categoria=this_cat)
    return render(request, "Jaguarete/busqueda.html",{"listcat":listaCategorias, "list_prod":listaProductos, "palabra":this_cat})

def producto(request,prod_id):
    listaCategorias = Categoria.objects.all()
    prod_view = get_object_or_404(Producto,id=prod_id)
    return render(request, "Jaguarete/Producto.html",{"listcat":listaCategorias, "this_prod":prod_view})

@login_required
def agregarproducto(request):
    listaCategorias = Categoria.objects.all()
    return render(request, "Jaguarete/agregarproducto.html",{"listcat":listaCategorias})

@login_required
def editarproductos(request):
    listaCategorias = Categoria.objects.all()
    return render(request, "Jaguarete/editarproductos.html",{"listcat":listaCategorias})

@login_required
def carrito(request):
    if request.user.is_superuser:
        return HttpResponseRedirect('/')
    else:
        listaCategorias = Categoria.objects.all()
    return render(request, "Jaguarete/carrito.html",{"listcat":listaCategorias})

def acercade(request):
    listaCategorias= Categoria.objects.all()
    return render(request, "Jaguarete/Acercade.html",{"listcat":listaCategorias})
    
