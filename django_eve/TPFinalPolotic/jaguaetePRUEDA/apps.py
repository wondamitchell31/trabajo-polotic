from django.apps import AppConfig


class JaguaetepruedaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jaguaetePRUEDA'
