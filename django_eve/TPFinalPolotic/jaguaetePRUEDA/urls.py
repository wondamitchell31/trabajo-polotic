from django.urls import path
from . import views

app_name = "jaguaetePRUEDA"

urlpatterns = [
    path('',views.inicio, name="inicio"),
    path('login', views.login, name="login"),
    path('registro',views.registro, name="registro"),
    path('busqueda',views.busqueda, name="busqueda"),
    path('categoria/<int:cat_id>',views.filtro_cat, name="filtro_categoria"),
    path('producto',views.producto, name="producto"),
    path('producto/<int:prod_id>',views.producto, name="Producto"),
    path('agregarproducto',views.agregarproducto, name="agregarproducto"),
    path('editarproductos',views.editarproductos, name="editarproductos"),
    path('carrito',views.carrito, name="carrito"),
    path('acercade',views.acercade, name="acercade"),
]