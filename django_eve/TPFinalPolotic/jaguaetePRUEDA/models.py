from django.db import models
from django.db.models.expressions import OrderBy

# Create your models here.
class Categoria(models.Model):
    descripcion = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.descripcion}"

class Producto(models.Model):
    titulo = models.CharField(max_length=20,null=True)
    imagen = models.ImageField(null=True)
    descripcion = models.CharField(max_length=50)
    precio = models.IntegerField()
    #slug = models.SlugField(max_length=200,db_index=True, null=True)
    categoria = models.ForeignKey(Categoria,on_delete=models.CASCADE,related_name="Productos")

    #agregado y el slug tambien
    #class Meta:
     #   index_together = (('id','slug'))
    #fin de agregado

    def __str__(self):
        return f"Producto {self.id}:({self.categoria}) {self.titulo} - {self.descripcion} --- ${self.precio}"

class Carrito(models.Model):
    #Usuario = models.ForeignKey(User,on_delete=models.CASCADE,related_name="usuario") 
    ListaProductos = models.ManyToManyField(Producto,blank=True,related_name="Compras")
    TotalCarrito = models.IntegerField(null=True)

    def __str__(self):
        return f"Carrito de {self.Usuario} ${self.TotalCarrito}"